<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/appointment');
});

Route::get('appointment','AppointmentController@Index');
Route::post('bookappointment','AppointmentController@Book');
Route::get('all_appointment','AppointmentController@View');
Route::get('getappointment/{page}/{recordsperpage}','AppointmentController@GetAppointment');
Route::get('countappointment','AppointmentController@CountAppoinment');