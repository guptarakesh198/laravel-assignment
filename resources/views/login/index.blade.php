@extends('layout.master')


@section('validation_error')

<div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 10px;margin-top: 10px;">

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

</div>
@endsection

@section('content')

 <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 30px;">

  <h2>Login Form</h2>

  <form action="{{ url('/login') }}" method="post">

  	{{ csrf_field() }}
    <div class="form-group">
      <label for="username">Username :</label>
      <input type="text" class="form-control" id="username" placeholder="Enter name" name="username">
    </div>

    <div class="form-group">
      <label for="password">Password :</label>
      <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password">
    </div>    

    <button type="submit" class="btn btn-default">Login</button>

  </form>

  </div>

@endsection