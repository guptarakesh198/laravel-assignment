@extends('layout.master')


@section('validation_error')

 <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 10px;margin-top: 10px;">

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

</div>
@endsection


@section('content')

 <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 30px;">

  <h2>Student Form</h2>

  <form action="{{ url('/studentadd') }}" method="post">

  	{{ csrf_field() }}
    <div class="form-group">
      <label for="name">Name :</label>
      <input type="text" class="form-control" id="name" placeholder="Enter name" name="sname">
    </div>

    <div class="form-group">
      <label for="std_class">Std :</label>
      <input type="text" class="form-control" id="std_class" placeholder="Enter Std" name="std_class">
    </div>    

    <button type="submit" class="btn btn-default">Insert</button>

  </form>

  </div>

@endsection


@section('studentlist')

<div class="col-lg-6 col-lg-offset-3">

<table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Std</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

    @foreach($students as $student)
      <tr>
        <td>{{ $student->sname }}</td>
        <td>{{ $student->std_class }}</td>
        <td>
        	<a href="{{ url('/studentedit/'.$student->id) }}" class="btn btn-xs btn-primary">Edit</a>&nbsp;
        	<a href="{{ url('/studentdelete/'.$student->id) }}" class="btn btn-xs btn-danger">Delete</a>
        </td>
      </tr>
    @endforeach

    </tbody>
  </table>
</div>

@endsection