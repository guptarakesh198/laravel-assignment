<!DOCTYPE html>
<html lang="en">
<head>
  <title>{{ $title or 'Appointment Assignment' }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/css/bootstrap-theme.min.css') }}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
  <link rel="stylesheet" href="https://unpkg.com/pc-bootstrap4-datetimepicker@4.17.50/build/css/bootstrap-datetimepicker.min.css">
  <script src="{{ asset('public/js/jquery.min.js') }}"></script>
  <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
  <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('public/js/vue.js') }}"></script>
  <script src="{{ asset('public/js/axios.min.js') }}"></script>
  <script src="https://unpkg.com/moment@2.18.1/min/moment.min.js"></script>
  <script src="https://www.malot.fr/bootstrap-datetimepicker/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js?t=20130302"></script>
  <script src="https://unpkg.com/pc-bootstrap4-datetimepicker@4.17.50/build/js/bootstrap-datetimepicker.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>
  <script src="https://unpkg.com/vuejs-datepicker"></script>
  <script src="https://unpkg.com/vue-bootstrap-datetimepicker@5.0.0-beta.2/dist/vue-bootstrap-datetimepicker.min.js"></script>
</head>
<body>
<div class="container" id="app">
<div class="row">
@include('flash-message')
@yield('validation_error')
@yield('content')
@yield('appointmentlist')
</div>
</div>
</body>
@yield('vuescript')
</html>