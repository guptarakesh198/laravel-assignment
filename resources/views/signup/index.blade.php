@extends('layout.master')


@section('validation_error')

 <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 10px;margin-top: 10px;">

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

</div>
@endsection


@section('content')

 <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 30px;">

  <h2>Email Form</h2>

  <form action="{{ url('/sendmail') }}" method="post">

  	{{ csrf_field() }}

    <div class="form-group">
      <label for="name">Name :</label>
      <input type="text" class="form-control" id="name" placeholder="Enter name" name="name">
    </div>

    <div class="form-group">
      <label for="name">Phone No :</label>
      <input type="text" class="form-control" id="no" placeholder="Enter Phone No" name="no">
    </div>


    <div class="form-group">
      <label for="name">Email :</label>
      <input type="text" class="form-control" id="email" placeholder="Enter Email" name="email">
    </div>
   

    <button type="submit" class="btn btn-default">Send Mail</button>

  </form>

  </div>

@endsection