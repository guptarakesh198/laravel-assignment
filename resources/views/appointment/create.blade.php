@extends('layout.master')


@section('validation_error')

<div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 10px;margin-top: 10px;">

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

</div>
@endsection


@section('content')

 <div class="col-lg-6 col-lg-offset-3" style="margin-bottom: 30px;">

  <center><h2>Book Appointment</h2></center>

  <form action="#" method="post" @submit.prevent="processForm">

  	{{ csrf_field() }}

    <div class="form-group" :class="{ 'form-group': true, 'has-error': errors.has('name') }">

      <label for="name">Name :</label>
      <input type="text" id="name" placeholder="Enter Name" name="name" :class="{ 'form-control': true, 'is-invalid': errors.has('name') }" v-model="name" v-validate="'required|checkuser'">

      <span v-show="errors.has('name')" class="help-block">@{{ errors.first('name') }}</span>

    </div>
    

    <div class="form-group" :class="{ 'form-group': true, 'has-error': errors.has('email') }">

      <label for="email">Email :</label>
      <input type="text" v-validate="'required|email'" :class="{ 'form-control': true, 'is-invalid': errors.has('email') }" id="email" placeholder="Enter Email" v-model="email" name="email">

       <span v-show="errors.has('email')" class="help-block">@{{ errors.first('email') }}</span>

    </div>   

    <div class="form-group" :class="{ 'form-group': true, 'has-error': errors.has('phone') }">
      <label for="phone">Phone :</label>
      <input type="text" v-validate="'required'" :class="{ 'form-control': true, 'is-invalid': errors.has('email') }" id="phone" placeholder="Enter Phone" v-model="phone" name="phone" v-mask="'(###) ###-####'">

       <span v-show="errors.has('phone')" class="help-block">@{{ errors.first('phone') }}</span>

    </div>  

    <div class="form-group" :class="{ 'form-group': true, 'has-error': errors.has('appointment_date') }">
      <label for="appointment_date">Appointment Date :</label>

        <vuejs-datepicker v-validate="'required'" :input-class="{ 'form-control': true, 'is-invalid': errors.has('appointment_date') }" id="appointment_date" placeholder="" v-model="appointment_date" :format="'MM/dd/yyyy'" name="appointment_date"></vuejs-datepicker>

     <span v-show="errors.has('appointment_date')" class="help-block">@{{ errors.first('appointment_date') }}</span>

    </div> 

    <div class="form-group" :class="{ 'form-group': true, 'has-error': errors.has('appointment_time') }">
      <label for="appointment_time">Appointment Time :</label>

      <date-picker v-validate="'required'" :class="{ 'timepicker form-control': true, 'is-invalid': errors.has('appointment_time') }" id="appointment_time" placeholder="" v-model="appointment_time" name="appointment_time" :config="options"></date-picker>
 
      <span v-show="errors.has('appointment_time')" class="help-block">@{{ errors.first('appointment_time') }}</span>

    </div>   

    <center>
    	<button id="button" :disabled="errors.any()" class="btn btn-primary btn-lg btn-block login-button">Book Appointment</button>
    </center>
    <br>
    <center>
    	<a id="button" href="{{ url('/all_appointment') }}" class="btn btn-success btn-lg btn-block login-button">View All Appointment</a>
    </center>

  </form>

  </div>



@endsection


@section('vuescript')

<script>

Vue.use(VueMask.VueMaskPlugin);
Vue.directive('mask', VueMask.VueMaskDirective);
Vue.use(VeeValidate);

Vue.component('date-picker', VueBootstrapDatetimePicker);
$.extend(true, $.fn.datetimepicker.defaults, {
  icons: {
    time: 'fa fa-clock',
    date: 'fa fa-calendar',
    up: 'fa fa-arrow-up',
    down: 'fa fa-arrow-down',
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-calendar-check',
    clear: 'fa fa-trash-o',
    close: 'fa fa-times-circle'
  }
});
//Vue.use(window.VueTimepicker);

const newrule = {
	  getMessage(field, params, data) {
	      return (data && data.message) || 'Something went wrong';
	  },
	  validate(value) {
	    return new Promise(resolve => {
	      resolve({
	        valid: ((value.length) < 3 || (value.length) > 255) ? false : !! value,
	        data: { message: `Name Supports Only 3 to 255 Characters` }
	      });
	    });
	  }
	};

VeeValidate.Validator.extend('checkuser',newrule);

    const App = new Vue({
        el: '#app',
        data: {
            name: null,
	    	email: null,
	    	phone : null,
	    	appointment_date : null,
	    	appointment_time : null,
	    	options: {
			      format: 'hh:mm:ss a',
			      useCurrent: false,
			      showClear: true,
			      showClose: true,
			    },
			addURL: '{{ url('/bookappointment') }}'
        },
         components: {
  				vuejsDatepicker
  				//VueTimepicker
  		},
      created: function(){
       
      },
        methods: {
            processForm: function() {

                this.$validator.validateAll().then((result) => {
                    if (result) {
                       	
		                axios.post(this.addURL,{
		                      name : this.name,
		                      email : this.email,
		                      phone : this.phone,
		                      appointment_date : this.appointment_date,
		                      appointment_time : this.appointment_time
		                    })
		                    .then(function (response){
		                        if(response.data.sucess == 'yes'){
		                      		alert('Appointment Booked');
		                        }else{
		                        	alert('Something Went Wrong.');
		                        }
		                        $('#addusernewform').modal('hide');

		                     })
		                    .catch(function(error){
		                        console.log(error);
		                     });
                       
                    }
                });

            }
        }

    });	
</script>

@endsection