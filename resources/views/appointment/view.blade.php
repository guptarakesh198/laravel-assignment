@extends('layout.master')

@section('appointmentlist')

<div class="col-lg-10 col-lg-offset-1" style="margin-top:30px;">

  <center>
      <a id="button" href="{{ url('/appointment') }}" class="btn btn-primary btn-lg btn-block login-button">Take New Appointment</a>
    </center>
    <br>
<table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Date</th>
        <th>Time</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="u in appointments">
          <td>@{{ u.id }}</td>
          <td>@{{ u.name }}</td>
          <td>@{{ u.email }}</td>
          <td>@{{ u.phone }}</td>
          <td>@{{ u.appointment_date }}</td>
          <td>@{{ u.appointment_time }}</td>
      </tr>
    </tbody>
  </table>

  <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
      <li v-for="n in pagecount" v-bind:class="{'page-item':true,'active':(n === currentpage)}">
          <a class="page-link" href="#" @click="activepage(n)">@{{ n }}</a>
      </li>
    </ul>
  </nav>

</div>

@endsection

@section('vuescript')


<script>

Vue.use('vue-moment');
var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    tURL : '{{ url('/getappointment') }}',
    totalURL : '{{ url('/countappointment') }}',
    pagecount : 0,
    totalrecords : 0,
    currentpage : 0,
    listperpage : 10,
    appointments : []
  },
  mounted(){

   axios.get(this.totalURL)
        .then(function (response){
            
            app.totalrecords = response.data.total;
            app.pagecount = Math.ceil(app.totalrecords/app.listperpage);
            console.log(app.totalrecords +' = '+ app.listperpage +' = '+ app.pagecount);
            app.activepage(1);

          })
        .catch(function(error){
            console.log(error);
         });

  },
 watch: {
    // whenever question changes, this function will run
    pagecount: function (newvalue, oldvalue) {
       app.pagecount = newvalue;
    }

  },
  methods:{
    refreshRecords : function(){

      axios.get(this.totalURL)
        .then(function (response){
            
            app.totalrecords = response.data.total;
            app.pagecount = Math.ceil(app.totalrecords/app.listperpage);
            app.activepage(app.currentpage);

          })
        .catch(function(error){
            console.log(error);
         });

    },
    activepage: function(page){

       app.currentpage = page;
       app.getusers();

    },
    getusers: function(){

        app.appointments = [];
        axios.get(this.tURL+'/'+ app.currentpage + '/' + app.listperpage)
          .then(function (response){
              for(var i = 0; i < response.data.length; i++) {
                app.appointments.push(response.data[i]);
              }

            })
          .catch(function(error){
              console.log(error);
           });

    }
  }
})

</script>

@endsection