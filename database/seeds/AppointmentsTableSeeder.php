<?php

use Illuminate\Database\Seeder;

class AppointmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('appointments')->delete();
        
        \DB::table('appointments')->insert(array (
            0 => 
            array (
                'id' => 3,
                'name' => 'Rakesh Gupta',
                'email' => 'guptarakesh198@gmail.com',
            'phone' => '(123) 232-3213',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:21:35 am',
                'created_at' => '2018-07-26 12:51:46',
                'updated_at' => '2018-07-26 12:51:46',
            ),
            1 => 
            array (
                'id' => 4,
                'name' => 'Rakesh Gupta',
                'email' => 'guptarakesh198@gmail.com',
            'phone' => '(123) 232-3213',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:24:51 am',
                'created_at' => '2018-07-26 12:55:17',
                'updated_at' => '2018-07-26 12:55:17',
            ),
            2 => 
            array (
                'id' => 5,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            3 => 
            array (
                'id' => 6,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            4 => 
            array (
                'id' => 7,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            5 => 
            array (
                'id' => 8,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            6 => 
            array (
                'id' => 9,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            7 => 
            array (
                'id' => 10,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            8 => 
            array (
                'id' => 11,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            9 => 
            array (
                'id' => 12,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            10 => 
            array (
                'id' => 13,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            11 => 
            array (
                'id' => 14,
                'name' => 'emp31',
                'email' => 'guptarakesh170@gmail.com',
            'phone' => '(123) 456-7890',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '06:33:02 pm',
                'created_at' => '2018-07-26 13:14:13',
                'updated_at' => '2018-07-26 13:14:13',
            ),
            12 => 
            array (
                'id' => 15,
                'name' => 'Rakesh Gupta',
                'email' => 'guptarakesh198@gmail.com',
            'phone' => '(123) 232-3213',
                'appointment_date' => '07/29/2018',
                'appointment_time' => '08:10:38 am',
                'created_at' => '2018-07-28 14:41:00',
                'updated_at' => '2018-07-28 14:41:00',
            ),
        ));
        
        
    }
}