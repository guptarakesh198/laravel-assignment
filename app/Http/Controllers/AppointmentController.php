<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;

class AppointmentController extends Controller
{

    public function Index(){

    	$title = "Book My Appointment";
    	return view('appointment.create',['title' => $title]);
    }

    public function Book(Request $request){

    	$request->validate([
    		'name' => 'required',
    		'email' => 'required|email',
    		'phone' => 'required',
    		'appointment_date' => 'required',
    		'appointment_time' => 'required',
    	]);
    	
    	$data = $request->all();
    	$out = array();
    	$appointment = new Appointment;
    	$appointment->name = $data['name'];
    	$appointment->email = $data['email'];
    	$appointment->phone = $data['phone'];
    	$appointment->appointment_date = date("m/d/Y", strtotime($data['appointment_date']));
    	$appointment->appointment_time = $data['appointment_time'];
    	$appointment->save();
    	$out['sucess'] = 'yes';
    	return $out;

    }

    public function View(){

    	return view('appointment.view');
        
    }

    public function CountAppoinment(){

        $count = Appointment::count();
        $out = array();
        $out['total'] = $count;
        return $out;

    }

    public function GetAppointment($page,$recordsperpage = '5'){

        return $appointments = Appointment::offset(($page-1)*$recordsperpage)->limit($recordsperpage)->get()->toArray();

    }
}
