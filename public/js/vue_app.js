Vue.use(VueMask.VueMaskPlugin);
Vue.directive('mask', VueMask.VueMaskDirective);
Vue.use(VeeValidate);

//Vue.use(window.VueTimepicker);

	/*const newrule = {
	  getMessage(field, params, data) {
	      return (data && data.message) || 'Something went wrong';
	  },
	  validate(value) {
	    return new Promise(resolve => {
	      resolve({
	        valid: username.includes(value.toLowerCase()) ? false : !! value,
	        data: { message: `${value} has already been taken` }
	      });
	    });
	  }
	};
	*/

	//VeeValidate.Validator.extend('checkuser',newrule);


    const App = new Vue({
        el: '#app',
        data: {
            name: null,
	    	email: null,
	    	phone : null,
	    	appointment_date : null,
	    	appointment_time : null,
        },
         components: {
  				vuejsDatepicker
  				//VueTimepicker
  		},
      created: function(){
       
      },
        methods: {
            processForm: function() {

                this.$validator.validateAll().then((result) => {
                    if (result) {
                       
                       alert('Form validated succesfully');
                    }
                });
            }
        }

    });	